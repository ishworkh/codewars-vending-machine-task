var CsvParser = (function(){
'use strict';

    var
      DEFAULT_QUOTE = '"',
      DEFAULT_DELIMITER = ',',
      NEWLINE = '\n';

      function CustomError(){
          this.stack = (new Error(this.message)).stack;
      }

      function EndOfCsvError(){
        this.message = 'Csv text finished crawling';
        CustomError.call(this);
      }

      // return going through each character in csv
      function CsvCharacterCrawler(csv)
      {
        var fullCsv = csv || '';
        var index = 0;

        this.getNextCharacter = function()
        {
            var character = fullCsv.charAt(index);
            index++;
            if (index > fullCsv.length)
            {
                throw new EndOfCsvError();
            }
            return character;
        }
      }

      function InvalidArgumentsForRowValueException(){
          this.message = 'Invalid arguments given RowValue object constructor';
          CustomError.call(this);
      };

      // complete row value
      function CsvRowValue(value, lineNumber)
      {
        if (undefined == value || undefined == lineNumber)
        {
          throw new InvalidArgumentsForRowValueError();
        }

        this.value = value;
        this.lineNumber = lineNumber;
      }

      // Handle double quoted cases
      function RowValueDoubleQuoteSanitizer(quote, separator)
      {
        function _isDoubleQuote(char, nextChar, nextNextChar)
        {
            if (char == quote && nextChar == quote)
            {
                return true;
            }
            return false;
        }

        function _isLastCharInWord(charIndex, wordLength)
        {
          return charIndex == wordLength;
        }

        this.sanitize = function(rawValue){
            var sanitizedValue = '';
            var valueLength = rawValue.length;

            for(var count = 0; count < valueLength; count++)
            {
              var char = rawValue[count];
                if (_isDoubleQuote(char, rawValue[count + 1]) && !_isLastCharInWord(count+2, valueLength))
                {
                    sanitizedValue += unescape(quote);
                    count++;
                    continue;
                }

                if (char == quote)
                {
                  continue;
                }

                sanitizedValue += char;
            }
            return sanitizedValue;
        }
      }

      function RowValueNotCompleteError(){
        this.message = 'Row value not complete. Requires a delimiter or newline or endof csv to be completed';
        CustomError.call(this);
      };

      function InvalidConstructorArgumentsForCompleteCsvRowValueFactory(){
        this.message = 'Invalid constructor given for CompleteCsvRowValueFactory';
        CustomError.call(this);
      };

      // class that is responsible for validating completion and creating rowvalue
      function CompleteCsvRowValueFactory(delimiter, quote, newLine, rowValueDoubleQuoteSanitizer)
      {
          if (
            !delimiter || !quote || !newLine
            || !rowValueDoubleQuoteSanitizer || ! rowValueDoubleQuoteSanitizer instanceof RowValueDoubleQuoteSanitizer
          )
          {
              throw new InvalidConstructorArgumentsForCompleteCsvRowValueFactory();
          }

          var currentValue = '';
          var quoteClosed = true;
          var currentLine = 0;

          function _incrementLineNumber()
          {
            currentLine++;
          }

          function _resetValue()
          {
            currentValue = '';
          }

          function _handleNewLine()
          {
              _incrementLineNumber();
              _resetValue();
          }

          function _handleDelimiter()
          {
            _resetValue();
          }

          function _sanitizeQuote(rawValue)
          {
              return rowValueDoubleQuoteSanitizer.sanitize(rawValue);
          }

          function _createRowValueWithCurrentValues(){
              return new CsvRowValue(_sanitizeQuote(currentValue), currentLine);
          }

          this.createCompleteRowValue = function(nextChar)
          {
              if (null == nextChar && quoteClosed)
              {
                return _createRowValueWithCurrentValues();
              }

              if (nextChar == newLine && quoteClosed)
              {
                  var row = _createRowValueWithCurrentValues();
                  _handleNewLine();
                  return row;
              }

              if (nextChar == delimiter && quoteClosed)
              {
                var row = _createRowValueWithCurrentValues();
                _handleDelimiter();
                  return row;
              }

              if (nextChar == quote)
              {
                quoteClosed = !quoteClosed;
              }

              currentValue = currentValue + nextChar;
              throw new RowValueNotCompleteError();
          }
      }

      function InvalidRowValueError(){
        this.message = 'Expected RowValue object as a paramter.';
        CustomError.call(this);
      };

      function CompleteCsvRowsHolder()
      {
        var rowValues = [];

        this.addRowValue = function(rowValue)
        {
            if (!rowValue instanceof CsvRowValue){
              throw new InvalidRowValueError();
            }
            var lineNumber = rowValue.lineNumber;

            if (undefined == rowValues[lineNumber])
            {
              rowValues.push([]);
            }

            rowValues[lineNumber].push(rowValue);
        }

        this.getAsArray = function()
        {
          return rowValues.map(function(lineRowValues){
              return lineRowValues.map(function(rowValue){
                  return rowValue.value;
              });
          });

        }
      }

      function UnclosedQuoteError(){
          this.message ='There is an unclosed quote in csv string. Fix it.';
          CustomError.call(this);
      };

      function CsvParser(csv, delimiter, quote, newLine)
      {
          var csv = csv || '';
          var delimiter = delimiter || DEFAULT_DELIMITER;
          var quote = quote || DEFAULT_QUOTE;
          var newLine = newLine || NEWLINE;

          var crawler = new CsvCharacterCrawler(csv);
          var csvRowValuesHolder = new CompleteCsvRowsHolder();

          var rowValueDoubleQuoteSanitizer = new RowValueDoubleQuoteSanitizer(quote);
          var factory = new CompleteCsvRowValueFactory(delimiter, quote, newLine, rowValueDoubleQuoteSanitizer);

          this.parse = function(){

              function _addRowValue(nextChar)
              {
                var RowValue = factory.createCompleteRowValue(nextChar);
                csvRowValuesHolder.addRowValue(RowValue);
              }
              // go through all characters provided by crawler, pass it to factory to get a valid
              // row value
              while(true){
                try{
                    var nextChar = crawler.getNextCharacter();
                    _addRowValue(nextChar);
                }catch(e)
                {
                    if (e instanceof RowValueNotCompleteError)
                    {
                      continue;
                    }
                    if (e instanceof EndOfCsvError)
                    {
                      break;
                    }

                    throw new Error('Failed to parse csv because' + e.message);
                }
              }

              // end of csv neither have a delimiter or new line so the last row
              // is still missing to be added to the holder. So one more add is needed.
              try {
                  _addRowValue(null);
              } catch (err) {
                  if (err instanceof RowValueNotCompleteError)
                  {
                      throw new UnclosedQuoteError();
                  }
                  throw err;
              }

              return csvRowValuesHolder.getAsArray();
          }
      }

      return CsvParser;
})();

var txt = 'one,"","two "" wraps\nonto ""two"" lines",three\n4,,6';
var check = new CsvParser(txt);

console.log(check.parse());
