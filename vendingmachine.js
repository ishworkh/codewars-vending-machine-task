var VendingMachine = (function() {
  'use strict';

  var Errors = {
    CoinGroup: {
      InvalidCoinTypeInConstructorError: function() {
        this.message = 'Invalid coin type given to the constructor for CoinGroup';
        this.stack = (new Error(this.message).stack);
      },
      InsufficientCoinsToWithdrawError: function() {
        this.message = 'Insufficient coins quantity.';
        this.stack = (new Error(this.message).stack);
      }
    },
    Kassa: {
      CoinTypeNotFoundInKassaError: function(type) {
        this.message = 'Coin type not found in kassa of : ' + type;
        this.stack = (new Error(this.message).stack);
      }
    },
    Cashier: {
      InvalidArgumentsError: function() {
        this.message = 'Invalid constructor argument. Kassa object is required';
        this.stack = (new Error(this.message).stack);
      },
      InvalidCoinGroupTypeError: function(type) {
        this.message = 'Invalid coin group type: ' + type;
        this.stack = (new Error(this.message).stack);
      },
      InsufficientBalanceProvidedError: function(balance, price) {
        this.message = 'Insufficient balance provided: ' + balance + ' for price: ' + price;
        this.stack = (new Error(this.message).stack);
      }
    },
    ReturnAmountCoinGroupsCalculator: {
      InvalidConstructorAgrumentsError: function() {
        this.message = 'Invalid constructor argument. Kassa object is required';
        this.stack = (new Error(this.message).stack);
      }
    }
  }

  // CoinGroup holder for coins
  function CoinGroup(type, quantity) {
    var type, quantity;

    if (!type) {
      throw new Errors.CoinGroup.InvalidCoinTypeInConstructorError();
    }

    type = parseInt(type);
    quantity = parseInt(quantity) || 0;

    this.getType = function() {
      return type;
    }

    this.getQuantity = function() {
      return quantity;
    }

    this.getTotalValue = function() {
      return type * quantity;
    }

    this.depositCoins = function(depositQuantity) {
      quantity = quantity + depositQuantity;
    }

    this.withdrawCoins = function(withdrawQuantity, checkSufficiencyOnly) {
      if (undefined === checkSufficiencyOnly) {
        checkSufficiencyOnly = false;
      }
      if (quantity <= 0) {
        throw new Errors.CoinGroup.InsufficientCoinsToWithdrawError();
      }
      if (!checkSufficiencyOnly) {
        quantity = quantity - withdrawQuantity;
      }
    }
  }

  // Kassa holder for CoinGroups
  function Kassa(coinGroups) {
    coinGroups = coinGroups || {};

    function _getCoinGroup(type) {
      if (undefined !== coinGroups[type]) {
        return coinGroups[type];
      }
      throw new Errors.Kassa.CoinTypeNotFoundInKassaError(type);
    }

    this.getAllCoinGroups = function() {
      return coinGroups;
    }

    this.getCoinGroupOfType = function(type) {
      return _getCoinGroup(type);
    }

    this.deposit = function(depositCoinGroup) {
      try {
        var coinGroup = _getCoinGroup(depositCoinGroup.getType());
        coinGroup.depositCoins(depositCoinGroup.getQuantity());
      } catch (e) {
        coinGroups[depositCoinGroup.getType()] = depositCoinGroup;
      }
    }

    this.withDraw = function(withdrawCoinGroup) {
      var coinGroup = _getCoinGroup(withdrawCoinGroup.getType());
      coinGroup.withdrawCoins(withdrawCoinGroup.getQuantity());
    }
  }

  // Cashier: responsible for performing transaction i.e. deposit, withdrawing form kassa
  // and calculating return CoinGroups
  function Cashier(kassa, returnAmountCoinGroupsCalculator) {

    if (
      !kassa || !kassa instanceof Kassa ||
      !returnAmountCoinGroupsCalculator || !returnAmountCoinGroupsCalculator instanceof ReturnAmountCoinGroupsCalculator
    ) {
      throw new Errors.Cashier.InvalidArgumentsError();
    }

    var helpers = {
      _validateInputCoinTypes: function(kassa, inputCoinGroups) {
        for (var type in inputCoinGroups) {
          try {
            kassa.getCoinGroupOfType(type);
          } catch (e) {
            throw new Errors.Cashier.InvalidCoinGroupTypeError(type);
          }
        }
      },
      _calculateTotalInputBalance: function(price, inputCoinGroups) {
        var inputSumAmount = 0;
        for (var key in inputCoinGroups) {
          inputSumAmount += inputCoinGroups[key].getTotalValue();
        }
        return inputSumAmount;
      },
      _validateBalanceSufficiency: function(inputBalance, price) {
        if (price > inputBalance) {
          throw new Errors.Cashier.InsufficientBalanceProvidedError(inputBalance, price);
        }
      },
      _depositInputCoinGroupsToKassa: function(kassa, coinGroups) {
        for (var key in coinGroups) {
          kassa.deposit(coinGroups[key]);
        }
      },
      _calculateReturnCoinGroups: function(returnAmount) {
        return returnAmountCoinGroupsCalculator.getReturnCoinGroups(returnAmount);
      }
    }

    this.doTheTransaction = function(price, inputCoinGroups) {

      var inputBalance, returnAmount;
      inputBalance = helpers._calculateTotalInputBalance(price, inputCoinGroups);

      helpers._validateBalanceSufficiency(inputBalance, price);

      returnAmount = inputBalance - price;

      helpers._depositInputCoinGroupsToKassa(kassa, inputCoinGroups);

      return helpers._calculateReturnCoinGroups(returnAmount);
    }

    this.getCurrentCoins = function() {
      return kassa.getAllCoinGroups();
    }

  }

  // Reponsible for calculating return amount coingruops
  function ReturnAmountCoinGroupsCalculator(kassa) {
    if (!kassa || !kassa instanceof Kassa) {
      throw new Errors.ReturnAmountCoinGroupsCalculator.InvalidConstructorAgrumentsError();
    }

    var helpers = {
      _areArrayEqual: function(array1, array2){
          if (typeof array1 !== typeof array2)
          {
            return false;
          }

          var a1 = array1;
          var a2 = array2;

          if (array2.length > array1.length)
          {
              a1 = array2;
              a2 = array1;
          }

          for(var count = 0; count < a1.length; count++){
              if (a2.indexOf(a1[count]) === -1)
              {
                return false;
              }
          }
          return true;
      },
      _withdrawCoinsFromKassa: function(kassa, coinGroups) {
        for (var type in coinGroups) {
          kassa.withDraw(coinGroups[type]);
        }
      },
      _getSumOfValuesInCombination: function(combination) {
        var sum = 0;
        for (var count = 0; count < combination.length; count++) {
          sum += combination[count];
        }
        return sum;
      },
      _getRange: function(max) {
        var range = [];
        for (var count = 0; count <= max; count++) {
          range.push(count);
        }
        return range;
      }
    }

    function _getCombination(skipIndex, returnAmount, kassa) {
      var remainingAmount, tryCoinType, returnAmount;
      remainingAmount = tryCoinType = returnAmount;

      function _cloneKassa(kassa)
      {
          var clone = new Kassa();
          var coinGroups = kassa.getAllCoinGroups();

          for(var type in coinGroups)
          {
              clone.deposit(new CoinGroup(type, coinGroups[type].getQuantity()));
          }
          return clone;
      }

      var localKassa = _cloneKassa(kassa);

      var localCombination = [];
      var skipIndexRange = helpers._getRange(skipIndex);
      while (remainingAmount !== 0 && tryCoinType > 0) {
        for (tryCoinType = remainingAmount; tryCoinType >= 0; tryCoinType--) {
          try {
              var coinGroup = localKassa.getCoinGroupOfType(tryCoinType);

              // check sufficiency of the coins in this coinGroup
              coinGroup.withdrawCoins(1);

              // skip array indexes including a range to given skip index which increases
              // for each combination produced to build different combination
              if (skipIndexRange.indexOf(localCombination.length) !== -1 && skipIndex !== 0) {
                skipIndexRange.shift();
                throw new Error('beskipped');
              }

              localCombination.push(tryCoinType);
              remainingAmount -= tryCoinType;
              break;
          } catch (e) {
            if (e instanceof Error && e.message === "beskipped") {
              tryCoinType--;
            }
            continue;
          }
        }
      }

      return localCombination;
    }

    function _chooseBestCombination(combinations, returnAmount) {
      for (var count = 0; count < combinations.length; count++) {
        if (helpers._getSumOfValuesInCombination(combinations[count]) === returnAmount) {
          return combinations[count];
        }
      }
      return combinations[0];
    }

    this.getReturnCoinGroups = function(returnAmount) {

      var coinGroups = {};
      var combinations = [];

      while (true) {
        var combination = _getCombination(combinations.length, returnAmount, kassa);
        if (helpers._areArrayEqual(combination, combinations[combinations.length - 1]))
        {
            break;
        }
        combinations.push(combination);
      }

      var bestCombination = _chooseBestCombination(combinations, returnAmount);

      for (var count = 0; count < bestCombination.length; count++) {
        var coinType = bestCombination[count];
        var coinGroup = coinGroups[coinType];
        if (undefined === coinGroup) {
          coinGroups[coinType] = new CoinGroup(coinType, 1);
        } else {
          coinGroup.depositCoins(1);
        }
      }
      helpers._withdrawCoinsFromKassa(kassa, coinGroups);

      return coinGroups;
    }
  }

  function VendingMachine(coins) {
    this.coins = coins;
    this.helpers = {
      mapCoinsObjectToCoinGroupsObject: function(coinsObject) {
        var result = {};
        for (var key in coinsObject) {
          result[key] = _createCoinGroup(key, coinsObject[key]);
        }
        return result;
      },
      mapCoinGroupsObjecttToCoinsObject: function(coinGroupsObject) {
        var result = {};
        for (var type in coinGroupsObject) {
          result[type] = coinGroupsObject[type].getQuantity();
        }
        return result;
      }
    }

    function _createCoinGroup(type, quantity) {
      return new CoinGroup(type, quantity);
    }

    function _createCashier(coinGroups) {
      var kassa = new Kassa(coinGroups);
      var returnAmountCoinGroupsCalculator = new ReturnAmountCoinGroupsCalculator(kassa);
      return new Cashier(kassa, returnAmountCoinGroupsCalculator);
    }

    this.cashier = _createCashier(
      this.helpers.mapCoinsObjectToCoinGroupsObject(coins)
    );
  }

  VendingMachine.prototype.vending = function(price, coins) {

    var inputCoinGroups = this.helpers.mapCoinsObjectToCoinGroupsObject(coins);
    try {
      var exchangeCoinGroups = this.cashier.doTheTransaction(price, inputCoinGroups);
      var coins = this.helpers.mapCoinGroupsObjecttToCoinsObject(exchangeCoinGroups);

      this.coins = this.helpers.mapCoinGroupsObjecttToCoinsObject(this.cashier.getCurrentCoins());
      return coins;
    } catch (e) {
      console.log(e.message);
      return coins;
    }
  }

  return VendingMachine;
})();

vm = new VendingMachine({1:0, 3:3, 5:2, 8:1})
console.log(vm.vending(11, {8:2}));
